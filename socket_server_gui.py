import socket
import wx


class myFrame(wx.Frame):
    def __init__(self, parent, id):
        wx.Frame.__init__(self, parent, id, title="服务器端", pos=(0, 0), size=(800, 600))
        panel = wx.Panel(self)
        # 创建容器
        hsizer_output = wx.BoxSizer(wx.HORIZONTAL)
        vsizer_all = wx.BoxSizer(wx.VERTICAL)
        # 创建静态提示文本
        self.title = wx.StaticText(panel, label="欢迎使用服务器端@_@")
        self.label_output = wx.StaticText(panel, label="客户端发送来了以下内容:\t")
        # 创建输入栏
        self.text_trans = wx.TextCtrl(panel, style=wx.TE_READONLY)
        # 将项目添加到容器中
        hsizer_output.Add(self.label_output, proportion=0, flag=wx.ALL, border=5)
        hsizer_output.Add(self.text_trans, proportion=1, flag=wx.ALL, border=5)
        # 将小容器添加到大容器中
        vsizer_all.Add(self.title, proportion=0, flag=wx.BOTTOM | wx.Top | wx.ALIGN_CENTER, border=45)
        vsizer_all.Add(hsizer_output, proportion=0, flag=wx.EXPAND | wx.LEFT | wx.RIGHT, border=45)
        panel.SetSizer(vsizer_all)


if __name__ == '__main__':
    app = wx.App()
    frame = myFrame(parent=None, id=-1)
    frame.Show()
    app.MainLoop()
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("127.0.0.1", 1000))
    s.listen()
    conn, address = s.accept()
    data = conn.recv(1024)
    if len(data) >= 1:
        conn.sendall(("服务器收到了你发送的内容：\n" + str(data.decode()) + "\n").encode())
        myFrame.text_trans.AppendText(data)
    exit()
