"""
#作者：20183122陈介
#创作日期：2020-3-18
#程序功能：优酷高评分电影
"""

#建立列表
movie = [("我们的父辈3",9.6,"菲利普"),
        ("我们的父辈2",9.6,"菲利普"),
        ("我们的父辈1",9.5,"菲利普"),
        ("三傻大闹宝莱坞", 9.4, "拉库马"),
        ("乱世佳人", 9.4, "乔治"),
        ("机器人总动员", 9.4, "安德鲁"),
        ("厉害了，我的国", 9.4, "卫铁"),
        ("城市之光", 9.4, "查理"),
        ("人生果实", 9.6, "伏原健之"),
        ("十二怒汉", 9.5, "西德尼")]

#排序前输出
print("\t优酷评分前十的电影(未按照评分高低排序)\t")
print ("      电影名       评分     导演")
for index,item in enumerate(movie):
    print (index+1,':',end="")
    for i in range(0,3):
        print (''.join(str(item[i]))+"\t\t",end="")
    print ("\n",end="")
print ("\n")

#对列表进行增，删，改
del movie[7]                             #按照位置删除一个元素
movie.append(("无间道",9.4,"刘伟强"))      #将一个元素增加到最后
movie[3]=("疯狂动物城",9.4,"拜恩")         #按照索引来改一个元素

#排序以及排序后输出
new_movie = sorted(movie,key=lambda s: s[1],reverse=True)
print("优酷评分前十的电影(修改及按评分排序后)\t")
print ("名次  电影名       评分     导演")
for index,item in enumerate(new_movie):
    print (index+1,':',end="")
    for i in range(0,3):
        print (''.join(str(item[i]))+"\t\t",end="")
    print ("\n",end="")