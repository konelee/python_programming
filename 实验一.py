"""
作者：20183122陈介
创作日期：2020-4-11
实验一
"""
def type_():
    a = 123
    b= '456'
    c = input("请输入数字：")
    d = int(input("请输入数字："))
    print(type(a))
    print(type(b))
    print(type(c))
    print(type(d))
def char():
    str1 = "人生苦短，我用python!"
    str2 = " 人生 如 逆旅\n你亦 是行人\n"
    str_en = "Life is a journey, you are a pedestrian."
    set1 = {1: [2, 'you'], 2: 12, 3: 'me'}
    print(type(set1))
    print(zip(set1))
    # 字符串的长度
    print(len(str1))
    print(len(str1.encode("utf-8")))
    print(len(str1.encode("GBK")))
    print("\n")
    # 字符段的截取
    print(str1[3])
    print(str1[2:8:2])
    print(str1[::3])
    try:
        print(str1[30])
    except IndexError:
        print("字符不存在")
    print("\n")
    # 字符串的分割
    str3 = str2.split()
    print(str3)
    str3 = str2.split(' ', )
    print(str3)
    str3 = str2.split(' ', 2)
    print(str3)
    print('\n')
    # 字符串的合并
    list1 = ['wyn', 'wbh', 'cxt', 'zjh']
    str3 = "\t@".join(list1)
    print('@' + str3)
    str3 = " ".join('abcdefg')
    print(str3)
    print('\n')
    # 字符串的检索
    print(str2.count('人', 0, 5))
    print(str2.count('人'))
    print(str2.find('人', 0, 5))
    print(str2.find('人'))  # 子字符串首次出现的位置
    print(str2.rfind('人', 0))
    print(str2.rfind('人'))
    print(str2.find('%'))
    print('人' in str2)
    print('$' in str2)
    print(str2.index('人', 0, 5))
    print(str2.index('人'))  # 子字符串首次出现的位置
    print(str2.rindex('人', 0))
    print(str2.rindex('人'))
    print(str2.startswith('人'))
    print(str2.startswith('生'))
    print(str2.startswith('生', 1))
    print(str2.endswith('人'))
    print(str2.endswith('行'))
    print(str2.endswith('行', 0, 13))
    print('\n')
    # 字母的大小写转换
    print(str_en.upper())
    print(str_en.lower())
    # 去除字符串中的特殊字符
    print(str2.strip())
    print(str2.lstrip())
    print(str2.rstrip())
    # 字符的编码
    byte = str1.encode('GBK')
    print(byte)
    print(str1.encode("UTF-8"))
    print('\n')
    # 字符的解码
    print(byte.decode("gbk"))
def object():
    #面对对象的编程
    class mate:
        name = "佚名"
        age = 18

        def __init__(self):
            print('姓名:', mate.name, '年龄：', mate.age)
            mate.school = "besti"
    wyn = mate()
    print(wyn.school)

    class student:
        stage = None
        name = '佚名'
        gender = None

        def __init__(self, in_stage, in_name, in_gender):
            student.stage = in_stage
            student.name = in_name
            student.gender = in_gender

        def _load(self):
            print("姓名:", student.name, "学历:", student.stage, "性别:", student.gender)

    class undergraduate(student):
        stage = "大学"
        age = 0
        prise = []

        def __init__(self, in_age):
            undergraduate.age = in_age

        def _load(self):
            print("姓名:", undergraduate.name, "学历:", undergraduate.stage, "性别:", undergraduate.gender, "年龄:",
                  undergraduate.age, "获奖：", undergraduate.prise)

        def __take(self, *in_prise):
            undergraduate.prise.append(in_prise)

    stu1 = student("高中", "赵四", "男")
    stu1._load()
    stu2 = student('大学', '张三', '男')
    stu2._load()
    stu3 = undergraduate(20)
    stu3._load()
    stu3._undergraduate__take("一等奖学金", "国家创新进步奖")
    stu3._load()
def annotation():
    pass
    #这是一行单行注释
    """
    这是多行注释的第一行
    这是多行注释的第二行
    下面可能还有好多行
    """
type_()
char()
object()
annotation()
