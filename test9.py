import sqlite3
#创建连接对象
conn = sqlite3.connect('classsmate.db')
#创建游标对象
cursor = conn.cursor()
#执行sqlite语句
cursor.execute('create table if not exists student (id int(10) primary key,name varchar(20),sex varchar(10))')
data = [(20181101,"张三","male"),(20181102,"李四","female"),(20181103,"王五","male")]
cursor.executemany('insert into student(id,name,sex) values (?,?,?)',data)
cursor.execute('insert into student(id,name,sex) values (?,?,?)',(20181104,"林六","male"))
cursor.execute('update student set sex = ? where id = ?',("female",20181101))
cursor.execute('delete from student where id = ?',(20181103,))  #王五被删除了
cursor.execute('select * from student ')
res = cursor.fetchall()
print("修改后的数据库内容：")
for item in res:
    print(item)
#关闭游标
cursor.close()
#提交事务
conn.commit()
#关闭连接
conn.close()