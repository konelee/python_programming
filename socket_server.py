import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(("127.0.0.1", 1000))
s.listen()
conn, address = s.accept()
data = conn.recv(1024)
print("服务器收到了", address[0], "的原始数据：", data)
conn.sendall(("服务器收到了你发送的数据：" + str(data.decode())).encode())
s.close()