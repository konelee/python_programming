"""
作者：20183122陈介
创作日期：2020-4-11
实验二：编写一个计算器
"""

def calculate1():
    try:
        operator1 = float(input("请输入操作数1："))
        operator2 = float(input("请输入操作数2："))
        way = input("请输入运算符(加：+  减：-  乘：*  除：/  求余：%  整除：//)：")
        result = {'+':operator1 + operator2,'-':operator1 - operator2,'*':operator1 * operator2,
                  '/':operator1 / operator2,'%':operator1 % operator2,'//':operator1 // operator2}
        print (operator1,way,operator2,"=",result[way])
        return result[way]
    except ZeroDivisionError:
        print ('@'*5+"除数不能为零！！！"+'@'*5)
    except ValueError:
        print("您输入的数据有误")

def calculate2():
    operator1 = int(input("请输入操作数1："))
    operator2 = int(input("请输入操作数2："))
    print (operator1,"^",operator2,"=",operator1 ** operator2)
    return operator1 ** operator2

def calculate3():
    operator1 = input("请输入操作数1：")
    operator2 = input("请输入操作数2：")
    operator1_new = eval(operator1)
    operator2_new = eval(operator2)
    way = input("请输入运算符(与：&  或：|  异或：^)：")
    result = {'&':operator1_new & operator2_new,'|':operator1_new | operator2_new,'^':operator1_new ^ operator2_new}
    print (operator1,way,operator2,"=",result[way])

def calculate4():
    operateor = input("请输入操作数：")
    operateor_new = eval(operateor)
    way = input("请输入运算符（非：~  左移：<<  右移：>>）")
    result = {'~':~operateor_new,'<<':operateor_new<<1,'>>':operateor_new>>1}
    print (operateor,"的运算结果为：",result[way])

def calculate5():
    operateor1 = input("请输入操作数：")
    operateor1_new = eval(operateor1)
    way = int(input("请输入您的目标进制:"))
    result = {2: bin(operateor1_new), 8: oct(operateor1_new), 16: hex(operateor1_new),10:int(operateor1_new)}
    print (operateor1,"的",way,"进制数为:",result[way])

def meun():
    print("*"*50)
    print("0:进行四则运算与求余整除运算")
    print("1:进行乘方运算")
    print("2:进行两个数的位运算")
    print("3:进行一个数的位运算")
    print("4:进行进制转化")
    print("除上以外任何输入:退出计算器")
    choose = int(input("请输入您的选择："))
    if choose == 0:
        temp = calculate1()
    elif choose == 1:
        temp = calculate2()
    elif choose == 2:
        temp = calculate3()
    elif choose == 3:
        temp = calculate4()
    elif choose == 4:
        temp = calculate5()
    else:
        print("感谢您的使用>_<")
        exit(0)

while(1):
    meun()