import socket
import wx


def mysend(text_input):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("127.0.0.1", 1000))
    s.sendall(str(text_input).encode())
    data = s.recv(1024)
    s.close()
    return data


class myFrame(wx.Frame):
    def __init__(self, parent, id):
        wx.Frame.__init__(self, parent, id, title="客户端", pos=(0, 0), size=(800, 600))
        panel = wx.Panel(self)
        # 创建容器
        hsizer_input = wx.BoxSizer(wx.HORIZONTAL)
        hsizer_output = wx.BoxSizer(wx.HORIZONTAL)
        hsizer_button = wx.BoxSizer(wx.HORIZONTAL)
        vsizer_all = wx.BoxSizer(wx.VERTICAL)
        # 创建按钮
        self.button_confirm = wx.Button(panel, label="发送")
        self.button_confirm.Bind(wx.EVT_BUTTON, self.OnclickSubmit)
        self.button_cancel = wx.Button(panel, label="取消")
        self.button_cancel.Bind(wx.EVT_BUTTON, self.OnclickCancel)
        # 创建静态提示文本
        self.title = wx.StaticText(panel, label="欢迎使用本客户端@_@")
        self.label_input = wx.StaticText(panel, label="请输入要发送给服务器的内容:")
        self.label_output = wx.StaticText(panel, label="服务器发送来了以下内容:\t",pos=(50,150))
        # 创建输入栏
        self.text_input = wx.TextCtrl(panel, style=wx.TE_LEFT)
        self.text_trans = wx.TextCtrl(panel, style=wx.TE_MULTILINE | wx.TE_WORDWRAP | wx.TE_READONLY | wx.TE_RICH2,pos=(200,150),size=(300,200))
        # 将项目添加到容器中
        hsizer_button.Add(self.button_confirm, proportion=0, flag=wx.ALL, border=5)
        hsizer_button.Add(self.button_cancel, proportion=0, flag=wx.ALL, border=5)
        # 将项目添加到容器中
        hsizer_input.Add(self.label_input, proportion=0, flag=wx.ALL, border=5)
        hsizer_input.Add(self.text_input, proportion=1, flag=wx.ALL, border=5)
        hsizer_output.Add(self.label_output, proportion=0, flag=wx.ALL, border=5)
        hsizer_output.Add(self.text_trans, proportion=1, flag=wx.ALL, border=5)
        # 将小容器添加到大容器中
        vsizer_all.Add(self.title, proportion=0, flag=wx.BOTTOM | wx.Top | wx.ALIGN_CENTER, border=15)
        vsizer_all.Add(hsizer_input, proportion=0, flag=wx.EXPAND | wx.LEFT | wx.RIGHT, border=45)
        vsizer_all.Add(hsizer_button, proportion=0, flag=wx.TOP | wx.ALIGN_CENTER | wx.BOTTOM, border=15)
        vsizer_all.Add(hsizer_output, proportion=0, flag=wx.EXPAND | wx.LEFT | wx.RIGHT, border=45)
        panel.SetSizer(vsizer_all)

    def OnclickCancel(self, event):
        self.text_input.SetValue("")

    def OnclickSubmit(self, event):
        data = mysend(self.text_input.GetValue())
        # wx.MessageBox(data)
        self.text_trans.AppendText(data.decode())


if __name__ == '__main__':
    app = wx.App()
    frame = myFrame(parent=None, id=-1)
    frame.Show()
    app.MainLoop()
