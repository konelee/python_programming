"""
#作者：20183122陈介
#创作日期：2020-4-1
#程序功能：根据生日求星座
"""
def function(month,day):
    """
    功能：根据生日求星座
    输入：生日
    返回:星座
    """
    date = [20, 19, 20, 20, 21, 21, 22, 23, 23, 23, 22, 21]
    if day <= date[month-1]:
        num = month - 1
    else:
        num = month
    return num

n = ('水瓶座','双鱼座','白羊座','金牛座','双子座','巨蟹座','狮子座','处女座','天秤座','天蝎座','射手座','摩羯座')
month = int(input("请输入您的出生月份:"))
day = int(input("请输入您的出生日期:"))
sign = function(month,day)
print ("您的星座是：",n[sign-1])