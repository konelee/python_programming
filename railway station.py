"""
#作者：20183122陈介
#创作日期：2020-3-26
#程序功能：实时显示火车信息
"""

import time
#定义表格打印函数
def list_print(list_information):
    print("车次      出发站-到达站    出发时间   到达时间     历时")  # 显示表头
    for items in list_information:
        print (items)

#初始表格

list_information = []
list_information.append("{:<6s}{:^15s}  {:0>5s}\t\t{:0>5s}\t   {:0>5s}".format("T40","长春-北京","00:12","12:20","12:08"))
list_information.append("{:<6s}{:^15s}  {:0>5s}\t\t{:0>5s}\t   {:0>5s}".format("T298","长春-北京","00:06","10:50","10:44"))
list_information.append("{:<6s}{:^15s}  {:0>5s}\t\t{:0>5s}\t   {:0>5s}".format("Z158","长春-北京","12:48","21:06","08:18"))
list_information.append("{:<6s}{:^15s}  {:0>5s}\t\t{:0>5s}\t   {:0>5s}".format("Z62","长春-北京","21:58","6:08","8:20"))
list_print(list_information)

flag = 1
while flag == 1:
    choose1 = input("请问您是否要对当前表格进行修改？(YES/NO)")
    choose1=choose1.lower()
    if choose1 == 'yes':
        flag2 = 1
        while flag2 == 1:
            num = int(input('您要修改第几项?'))
            temp1 = input("请输入车次:")
            temp2 = input("请输入出发站-到达站:")
            temp3 = input("请输入出发时间:")
            temp4 = input("请输入到达时间:")
            temp5 = input("请输入历经时长:")
            list_information[num-1] = "{:<6s}{:^15s}  {:0>5s}\t\t{:0>5s}\t   {:0>5s}".format(temp1,temp2,temp3,temp4,temp5)
            list_print(list_information)
            ans = input("请问您是否要继续修改:(输入任何YES以外的字符串将退出修改)")
            ans = ans.lower()
            if ans == "yes":
                pass
            else:
                flag2 = 0
            flag = 0
    elif choose1 == 'no':
        print("感谢您的使用，系统即将退出\n",end='')
        for i in range(7):
            time.sleep(0.5)
            print (".",end="")
        flag = 0
    else:
        print ("您的输入有误,请重新输入:")