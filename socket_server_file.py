import socket


def read():
    with open("server_data.txt", "r") as file:
        content: str = file.read()
    return content


if __name__ == '__main__':
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("127.0.0.1", 1000))
    while 1:
        s.listen()
        conn, address = s.accept()
        trans = read()
        if conn.sendall(("服务器发来了内容：\n" + str(trans)).encode()):
            print("已经将服务器文档发送给用户")
        print("*"*70)
        data = conn.recv(1024)
        print("服务器收到了", address[0], "的原始数据：\n", data.decode())
        conn.sendall(("服务器收到了你发送的内容：\n" + str(data.decode())).encode())
    s.close()