#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ author:konnlee
@ time  :2020/5/11 19:46
@ School:BESTI
"""
import os
import sys
import time
import sqlite3
from PyQt5.QtWidgets import QPushButton, QApplication, QToolTip, QWidget, QMessageBox, QMainWindow, QLineEdit
from PyQt5.QtWidgets import QTextEdit, QFileDialog, QFileDialog, QLabel,  QFormLayout, QPlainTextEdit, QMenu
from PyQt5.QtGui import QIcon, QFont, QPixmap


def look_table_ppt(word):
    info = []
    print("search word in:", word)
    conn = sqlite3.connect("source.db")
    cursor = conn.cursor()
    sql = "insert into ppt (id,des,url,kind) values(?,?,?,?)"
    data = [(1, 'EDA SOPC实验箱底板和核心板IO分配表.pdf',
             'https://www.mosoteach.cn/web/index.php?c=res&m=download&file_id=F52D0A05-D2F3-4245-AD1D-C7B91328F469&clazz_course_id=B902BBD8-9355-4D96-B7D9-35CD8ED91E73',
             'PPT'),
            (3, 'EDA实验箱八位七段数码管动态显示电路.docx', 'www.wendang/docx/897', 'DOC'),
            (4, '多层次的电路设计及调试', 'www.shiping.com/1', 'MP4'),
            (5, '芯片是如何设计出来的', 'www.shiping.com/LOOP/4', 'MP4')]
    cursor.executemany(sql, data)
    sql = 'select * from ppt where id < 10'
    cursor.execute(sql)
    res = cursor.fetchall()
    for item in res:
        if (word in item[1] or word in item[3]):
            search_info = ""
            search_info = search_info + "*"*50 + "\n"
            search_info = search_info + "描述：" + item[1] + "\n"
            search_info = search_info + "网址：" + item[2] + "\n"
            search_info = search_info + "类型：" + item[3] + "\n"
            search_info = search_info + "*"*50 + "\n"
            info.append(search_info)
    cursor.close()
    conn.close()
    return info


def get_url(index, word):
    title = '没查询到结果'
    kind = 'NULL'
    info = []
    if index == 1:
        url = 'www.bing.com'
        title = 'EDA SOPC实验箱底板和核心板IO分配表.pdf'
        kind = 'pdf'
        try:
            info = look_table_ppt(word)
        except Exception as e:
            print(e)
    elif index == 2:
        url = "www.baidu.com"
        look_table_vid(word)
    else:
        url = "www.runoob.com"
        look_table_exp(word)
    print(title)
    print(info)
    return url, title, kind, info


class Frame(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ppt = QLineEdit(self)
        self.vid = QLineEdit(self)
        self.exp = QLineEdit(self)
        self.main_window_init()
        self.button_set()
        self.search_info_set()

    def main_window_init(self):
        self.setGeometry(100, 100, 1600, 900)
        self.setWindowTitle("资料查找小工具by陈介")
        self.setToolTip("这是一个由<h5>陈介</h5>编写的程序")
        self.setWindowIcon(QIcon("OIP.jpg"))
        self.statusBar().showMessage('Running：数电\EDA课程资料查找工具')

    def button_set(self):
        ppt = QPushButton("课件查询", self)
        ppt.setToolTip("按下此按钮查询课件")
        ppt.sizeHint()
        ppt.move(1200, 200)
        ppt.clicked.connect(self.ppt_search)

        vid = QPushButton("视频查询", self)
        vid.setToolTip("按下此按钮查询视频")
        vid.sizeHint()
        vid.move(1200, 400)
        vid.clicked.connect(self.vid_search)

        exp = QPushButton("实验查询", self)
        exp.setToolTip("按下此按钮查询实验相关")
        exp.sizeHint()
        exp.move(1200, 600)
        exp.clicked.connect(self.exp_search)

        ex_button = QPushButton("按下退出程序", self)
        ex_button.setToolTip("按下此按钮退出程序")
        ex_button.sizeHint()
        ex_button.move(800, 800)
        ex_button.clicked.connect(self.close)

    def search_info_set(self):
        self.ppt.move(200, 200)
        self.ppt.setPlaceholderText("请输入查询关键词，支持','分隔")
        self.ppt.resize(1000, 30)

        self.vid.move(200, 400)
        self.vid.setPlaceholderText("请输入查询关键词，支持','分隔")
        self.vid.resize(1000, 30)

        self.exp.move(200, 600)
        self.exp.setPlaceholderText("请输入查询关键词，支持','分隔")
        self.exp.resize(1000, 30)

    def ppt_search(self):
        self.index = 0
        self.word = self.ppt.text()
        print(self.word)
        self.ppt_info = QPlainTextEdit(self)
        self.ppt_info.setReadOnly(True)
        self.ppt_info.setGeometry(200, 230, 1000, 170)
        self.ppt_info.show()
        url, title, kind, self.word = get_url(1, self.word)
        self.len = len(self.word)
        QMessageBox.information(self, "提示", "课件查找完毕")
        msg_herf = "<a href='" + url + "'>访问必应< /a>\n"
        self.ppt_info.setPlainText(self.word[self.index])
        # >>>
        last = QPushButton("上一条", self)
        last.sizeHint()
        last.move(1200, 280)
        last.show()
        last.clicked.connect(self.ppt_sub)
        next = QPushButton("下一条", self)
        next.sizeHint()
        next.move(1200, 320)
        next.show()
        next.clicked.connect(self.ppt_add)
        # >>>
        line = QLabel(self)
        line.setGeometry(1200, 230, 100, 50)
        line.setText(msg_herf)
        line.setOpenExternalLinks(True)
        line.show()

    def ppt_sub(self):
        if self.index == 0:
            QMessageBox.information(self, "警告", "已经是第一条结果！")
        else:
            self.index = self.index - 1
            self.ppt_info.setPlainText(self.word[self.index])

    def ppt_add(self):
        if self.index == self.len - 1:
            QMessageBox.information(self, "警告", "已经是最后一条结果！")
        else:
            self.index = self.index + 1
            self.ppt_info.setPlainText(self.word[self.index])

    def vid_search(self):
        self.index = 0
        self.word = self.vid.text()
        print(self.word)
        self.vid_info = QPlainTextEdit(self)
        self.vid_info.setReadOnly(True)
        self.vid_info.setGeometry(200, 430, 1000, 170)
        self.vid_info.show()
        url, title, kind, self.word = get_url(1, self.word)
        self.len = len(self.word)
        QMessageBox.information(self, "提示", "课件查找完毕")
        msg_herf = "<a href='" + url + "'>访问必应< /a>\n"
        self.vid_info.setPlainText(self.word[self.index])
        # >>>
        last = QPushButton("上一条", self)
        last.sizeHint()
        last.move(1200, 480)
        last.show()
        last.clicked.connect(self.vid_sub)
        next = QPushButton("下一条", self)
        next.sizeHint()
        next.move(1200, 520)
        next.show()
        next.clicked.connect(self.vid_add)
        # >>>
        line1 = QLabel(self)
        line1.setGeometry(1200, 430, 100, 50)
        line1.setText(msg_herf)
        line1.setOpenExternalLinks(True)
        line1.show()

    def vid_sub(self):
        if self.index == 0:
            QMessageBox.information(self, "警告", "已经是第一条结果！")
        else:
            self.index = self.index - 1
            self.vid_info.setPlainText(self.word[self.index])

    def vid_add(self):
        if self.index == self.len - 1:
            QMessageBox.information(self, "警告", "已经是最后一条结果！")
        else:
            self.index = self.index + 1
            self.vid_info.setPlainText(self.word[self.index])

    def exp_search(self):
        self.index = 0
        self.word = self.exp.text()
        print(self.word)
        self.exp_info = QPlainTextEdit(self)
        self.exp_info.setReadOnly(True)
        self.exp_info.setGeometry(200, 630, 1000, 170)
        self.exp_info.show()
        url, title, kind, self.word = get_url(1, self.word)
        self.len = len(self.word)
        QMessageBox.information(self, "提示", "课件查找完毕")
        msg_herf = "<a href='" + url + "'>访问必应< /a>\n"
        self.exp_info.setPlainText(self.word[self.index])
        # >>>
        last = QPushButton("上一条", self)
        last.sizeHint()
        last.move(1200, 680)
        last.show()
        last.clicked.connect(self.exp_sub)
        next = QPushButton("下一条", self)
        next.sizeHint()
        next.move(1200, 720)
        next.show()
        next.clicked.connect(self.exp_add)
        # >>>
        line1 = QLabel(self)
        line1.setGeometry(1200, 630, 100, 50)
        line1.setText(msg_herf)
        line1.setOpenExternalLinks(True)
        line1.show()

    def exp_sub(self):
        if self.index == 0:
            QMessageBox.information(self, "警告", "已经是第一条结果！")
        else:
            self.index = self.index - 1
            self.exp_info.setPlainText(self.word[self.index])

    def exp_add(self):
        if self.index == self.len - 1:
            QMessageBox.information(self, "警告", "已经是最后一条结果！")
        else:
            self.index = self.index + 1
            self.exp_info.setPlainText(self.word[self.index])


def Main_win_build():
    app = QApplication(sys.argv)
    frame = Frame()
    frame.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    Main_win_build()
