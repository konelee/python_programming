"""
作者：20183122陈介
创作日期：2020-4-8
面向对象的三要素：封装、继承、多态
"""
class student:
    stage = None
    name = '佚名'
    gender = None
    def __init__(self,in_stage,in_name,in_gender):
        student.stage = in_stage
        student.name = in_name
        student.gender = in_gender
    def _load(self):
        print ("姓名:",student.name,"学历:",student.stage,"性别:",student.gender)

class undergraduate(student):
    stage = "大学"
    age = 0
    prise = []
    def __init__(self,in_age):
        undergraduate.age = in_age
    def _load(self):
        print ("姓名:",undergraduate.name,"学历:",undergraduate.stage,"性别:",undergraduate.gender,"年龄:",undergraduate.age,"获奖：",undergraduate.prise)
    def __take(self,*in_prise):
        undergraduate.prise.append(in_prise)


stu1 = student("高中","赵四","男")
stu1._load()
stu2 = student('大学','张三','男')
stu2._load()
stu3 = undergraduate(20)
stu3._load()
stu3._undergraduate__take("一等奖学金","国家创新进步奖")
stu3._load()