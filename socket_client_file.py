import socket


def read():
    with open("client_data.txt", "r") as file:
        content: str = file.read()
    return content


if __name__ == '__main__':
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("127.0.0.1", 1000))
    trans = read()
    s.sendall(trans.encode())
    data = s.recv(1024)
    print(data.decode())
    print("*"*70)
    data = s.recv(1024)
    print(data.decode())
    s.close()
