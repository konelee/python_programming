"""
#作者：20183122陈介
#创作日期：2020-3-18
#程序功能：王者荣耀英雄职业
"""

#构建列表
cike = ["李白","孙悟空","阿柯","韩信","橘右京","司马懿"]
tank = ["廉颇","庄周","张飞","牛魔","程咬金","亚瑟"]
zhanshi = ["宫本武藏","曹操","达摩","花木兰","典韦","狂铁"]
fashi = ["小乔","妲己","诸葛亮","王昭君","武则天","甄姬"]
shooter = ["狄仁杰","李元芳","鲁班七号","公孙离","孙尚香","后裔"]
fuzhu = ["孙膑","瑶","明世隐","大乔","鬼谷子","蔡文姬"]

#对列表进行增，删，改
tank.append("猪八戒")
fuzhu.insert(3,"牛魔")
del zhanshi[1]
shooter.pop(3)
cike[4]="娜可露露"

#遍历输出列表
print ("刺客:")
for temp in cike:
    print (temp,"\t",end="")
print ("\n")
print ("坦克:")
for temp in tank:
    print (temp,"\t",end="")
print ("\n")
print ("战士:")
for temp in zhanshi:
    print (temp,"\t",end="")
print ("\n")
print ("法师:")
for temp in fashi:
    print (temp,"\t",end="")
print ("\n")
print ("射手:")
for temp in shooter:
    print (temp,"\t",end="")
print ("\n")
print ("辅助:")
for temp in fuzhu:
    print (temp,"\t",end="")
print ("\n")